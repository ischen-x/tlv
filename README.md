<!--
 * @Author: ischen.x ischen.x@foxmail.com
 * @Date: 2025-03-03 14:00:39
 * @LastEditors: ischen.x ischen.x@foxmail.com
 * @LastEditTime: 2025-03-04 13:55:11
 * 
 * Copyright (c) 2025 by fhchengz, All Rights Reserved. 
-->
# 基于TLV的XXX传输协议
||head|tag|length|value|crc|
|-|-|-|-|-|-|
|示例1|0x55AA|0x00|0x01|0x12|0xXX|
|示例2|0x55AA|0x01|0x02|0x1234|0xXX|

* head    帧头
* tag     帧类型，帧标识
* length  帧长度(仅指示value字段的长度)
* value   帧数据
* crc crc 校验数据


###### C语言表示
```c
#define FIELD_TYPE_HEAD     USE_UINT16_T
#define FIELD_TYPE_TAG      USE_UINT8_T
#define FIELD_TYPE_LENGTH   USE_UINT8_T
#define FIELD_TYPE_CRC      USE_UINT8_T
#define FIELD_TYPE_VALUE    USE_UINT8_T

// 根据宏定义选择不同的字段类型
#if FIELD_TYPE_HEAD == USE_UINT8_T
    typedef uint8_t head_type;
#elif FIELD_TYPE_HEAD == USE_UINT16_T
    typedef uint16_t head_type;
#elif FIELD_TYPE_HEAD == USE_UINT32_T
    typedef uint32_t head_type;
#else
    #error "Unsupported head type"
#endif
// ... 另外帧成员长度类似

// 帧结构
typedef struct _tlv {
    head_type head;            // 根据宏定义的类型
    tag_type tag;              
    length_type length;        
    value_type *value;         // 指向帧数据的指针
    crc_type crc;             
} tlv_frame_t;
```
